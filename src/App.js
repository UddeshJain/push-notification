import React from 'react';

function App() {

  const clickHandler = () => {
    Notification.requestPermission(result => {
      if (result === 'granted') {
        showNotification('So nice to have you here!', 'Hey there!')
      }
    });
    
    function showNotification(title, message) {
      if ('Notification' in window) {
        navigator.serviceWorker.ready.then(registration => {
          registration.showNotification(title, {
            body: message,
            tag: 'vibration-sample'
          });
        });
        console.log("pushed")
      }
    }
  }
  return (
    <div className="App">
      <h1>Test app for push notification</h1>
      <button onClick={clickHandler}>Click</button>
    </div>
  );
}

export default App;
